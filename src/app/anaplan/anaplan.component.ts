import { Component, ViewChild, OnInit } from '@angular/core';
import { PopupAnaplanComponent } from '../popup-anaplan/popup-anaplan.component';
import { MatDialog } from '@angular/material';
import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { MatTableDataSource, MatSort } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { AccountPreviewService } from '../salesforce/service/AccountPreview';

export interface PeriodicElement {
  Label: string;
  API_Name: string;
  Description: string;
  sync: string;
  Deployed: string;
}

export interface Elements {
  Global_VP: string;
  Geo_VP: string;
  Sub_Region:string;
  Sales_Rep_Role: string;
  Tensor_flow_Quota: number;
  Final_Quota: number;
  Bookings_in_Current_Fy: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { Label: 'Quota', API_Name: 'Quota Planning', Description: 'Brief Description of Quota Planning ', sync: '10/12/13', Deployed: 'yes' }
];


@Component({
  selector: 'app-anaplan',
  templateUrl: './anaplan.component.html',
  styleUrls: ['./anaplan.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AnaplanComponent implements OnInit {

  // constructor(public dialog: MatDialog, private readonly accountPreviewService: AccountPreviewService) { }
  // collapsed: false;

  columnsToDisplay: string[] = ['select', 'Label', 'API_Name', 'Description', 'sync', 'Deployed'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  expandedElement: PeriodicElement | null;

  datasources1 = ELEMENT_DATA1;
  displayedColumns: string[] = ['Global_VP', 'Geo_VP', 'Sub_Region','Sales_Rep_Role','Tensor_flow_Quota','Final_Quota', 'Bookings_in_Current_Fy'];
  expandedElements: Elements | null;

  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  dialog: any;
  openDialog(): void {
    const dialogRef = this.dialog.open(PopupAnaplanComponent, {
      width: '700px',
      height: '530px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.Label + 1}`;
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  //   this.accountPreviewService.getAll({})
  //     .subscribe(response => {
  //         this.datasources1 = response;
  //         this.displayedColumns  = ['Global_VP', 'Geo_VP', 'Sub_Region','Sales_Rep_Role','Tensor_flow_Quota','Final_Quota', 'Booking_in_current_FY'];
  //     });
  // }
  }
  }

  const ELEMENT_DATA1: Elements[] = [
    {
      Global_VP: 'Global VP 1',
      Geo_VP: 'EMEA',
      Sub_Region: 'SD-EMED',
      Sales_Rep_Role:'Enterprise',
      Tensor_flow_Quota:11200000,
      Final_Quota:11200000,
      Bookings_in_Current_Fy:10400000,
    }, 
    {
    Global_VP: 'Global vp 1',
  Geo_VP: 'EMEA',
  Sub_Region:'Israel',
  Sales_Rep_Role: 'Enterprise',
  Tensor_flow_Quota:11200000,
      Final_Quota:11200000,
      Bookings_in_Current_Fy:10400000,
    },  
     {
      Global_VP: 'Global vp 1',
      Geo_VP: 'EMEA',
      Sub_Region:'Israel Channel',
      Sales_Rep_Role: 'Territory',
      Tensor_flow_Quota:12600000,
          Final_Quota:12600000,
          Bookings_in_Current_Fy:11700000,
      
    },  
    {
      Global_VP: 'Global vp 1',
  Geo_VP: 'EMEA',
  Sub_Region:'TEST',
  Sales_Rep_Role: 'Enterprise',
  Tensor_flow_Quota:9800000,
      Final_Quota:9800000,
      Bookings_in_Current_Fy:9100000,
    }, 
    {
      Global_VP: 'Global vp 1',
  Geo_VP: 'EMEA',
  Sub_Region:'SD-Switzerland',
  Sales_Rep_Role: 'Territory',
  Tensor_flow_Quota:11200000,
      Final_Quota:11200000,
      Bookings_in_Current_Fy:10400000,
    }, 
    {
      Global_VP: 'Global vp 1',
      Geo_VP: 'EMEA',
      Sub_Region:'Switzerland SubDistrict',
      Sales_Rep_Role: 'Enterprise',
      Tensor_flow_Quota:12600000,
      Final_Quota:12600000,
      Bookings_in_Current_Fy:11700000,
  
    },
    {
      Global_VP: 'Global vp 1',
      Geo_VP: 'EMEA',
      Sub_Region:'HI',
      Sales_Rep_Role: 'Corporate',
      Tensor_flow_Quota:12600000,
      Final_Quota:12600000,
      Bookings_in_Current_Fy:11700000,
  
    },
    {
      Global_VP: 'Global vp 1',
      Geo_VP: 'EMEA',
      Sub_Region:'Germany SubDistrict',
      Sales_Rep_Role: 'Territory',
      Tensor_flow_Quota:12600000,
          Final_Quota:12600000,
          Bookings_in_Current_Fy:11700000,
      
    },
  ];