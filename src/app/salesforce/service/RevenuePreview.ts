import { Injectable } from '@angular/core';
import { AjaxService } from './../../shared/services/ajaxService.service';

@Injectable({
    providedIn: 'root',
  })

  export class RevenuePreviewService {
    constructor(private readonly _ajaxService: AjaxService) { }

    getAll(params: object): any {
        //return this._ajaxService.getWithParam(params, 'dashboard/sdfc-service.php');
        return this._ajaxService.getWithParam(params, 'api/RevenuePreview');
      }
    }